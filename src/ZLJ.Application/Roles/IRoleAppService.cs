﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using BXJG.GeneralTree;
using BXJG.Common.Dto;
using ZLJ.Roles.Dto;

namespace ZLJ.Roles
{
    public interface IRoleAppService : IAsyncCrudAppService<RoleDto, int, PagedRoleResultRequestDto, CreateRoleDto, RoleDto>
    {
        Task<ListResultDto<PermissionDto>> GetAllPermissions();

        Task<GetRoleForEditOutput> GetRoleForEdit(EntityDto input);

        Task<ListResultDto<RoleListDto>> GetRolesAsync(GetRolesInput input);
        Task<IReadOnlyList<RoleSelectDto>> GetForSelectAsync(GetForSelectInput a );
        Task<IEnumerable<int>> DeleteBatchAsync(params int[] input);
    }
}
